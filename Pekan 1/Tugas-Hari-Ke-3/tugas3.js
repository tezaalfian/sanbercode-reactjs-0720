// soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
var kalimat = kataPertama.concat(' ',kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1),' ',kataKetiga,' ',kataKeempat.toUpperCase());
console.log(kalimat);

// soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";
var hasil = parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat);
console.log(hasil);

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24);
var kataKelima = kalimat.substring(25,31);

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// soal 4
var nilai = 100;
var index;
if(nilai >= 80) {
    index = 'A';
}else if(nilai >= 70 && nilai < 80){ 
    index = 'B';
}else if(nilai >= 60 && nilai < 70){
    index = 'C';
}else if(nilai >= 50 && nilai < 60){
     index = 'D';
}else if(nilai < 50){
    index = 'E';
}
console.log(index);

// soal 5
var tanggal = 07;
var bulan = 10;
var tahun = 1999;
var month;
switch (bulan) {
    case 1: month = 'Januari'; break;
    case 2: month = 'Februari'; break;
    case 3: month = 'Maret'; break;
    case 4: month = 'April'; break;
    case 5: month = 'Mei'; break;
    case 6: month = 'Juni'; break;
    case 7: month = 'Juli'; break;
    case 8: month = 'Agustus'; break;
    case 9: month = 'September'; break;
    case 10: month = 'Oktober'; break;
    case 11: month = 'Nopember'; break;
    case 12: month = 'Desember'; break;
    default:
        break;
}
console.log(String(tanggal).concat(' ',month,' ',String(tahun)));