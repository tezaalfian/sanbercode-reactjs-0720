// soal 1
var i = 2;
console.log('LOOPING PERTAMA');
while (i <= 20) {
    console.log(i+' - I Live Coding');
    i+=2;
}
i = 20
console.log('LOOPING KEDUA');
while (i >= 2) {
    console.log(i+' - I will become a frontend developer');
    i-=2;
}

// soal 2
for (let i = 1; i <= 20; i++) {
    if(i%3 == 0 && i%2 == 1){
        console.log(i+" - I Love Coding");
    }else if (i % 2 == 0) {
        console.log(i+' - Berkualitas');
    } else if(i%2 == 1){
        console.log(i+' - Santai');
    }
}

// soal 3
for(let i = 1; i <= 7; i++){
    let output = '';
    for (let j = 0; j < i; j++) {
        output += '#';
    }
    console.log(output);
}

// soal 4
var kalimat="saya sangat senang belajar javascript";
var name = kalimat.split(' ');
console.log(name);

// soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var sortBuah = daftarBuah.sort();
for (let i = 0; i < sortBuah.length; i++) {
    console.log(sortBuah[i]);
}