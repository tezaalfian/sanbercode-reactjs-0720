// soal 1
const lingkaran = function(r){
    this.r = r;
    this.phi = 3.14;
    this.luas = () => this.phi*this.r*this.r;
    this.keliling = () => 2*this.phi*this.r;
}

const circle = new lingkaran(10);
console.log(circle.luas());
console.log(circle.keliling());

// soal 2
const gabung = (kata1,kata2,kata3,kata4,kata5) => {
    return `${kata1} ${kata2} ${kata3} ${kata4} ${kata5}`;
};
let kalimat = gabung('saya','adalah','seorang','frontend','developer');
console.log(kalimat);

// soal 3
class Book {
    constructor(name, totalPage, price){
        this.name = name;
        this.totalPage = totalPage;
        this.price = price;
    }
}

class Komik extends Book {
    constructor(name, totalPage, price,isColorful){
        super(name,totalPage,price);
        this.isColorful = isColorful;
    }
}

const book1 = new Book('Kalkulus',30,70000);
const komik1 = new Komik('Naruto',50,50000,false);
console.log(book1);
console.log(komik1);