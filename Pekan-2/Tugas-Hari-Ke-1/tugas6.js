// soal 1
var daftarPeserta = {
  nama: "Asep",
  jenis_kelamin: "Laki-Laki",
  hobi: "Baca Buku",
  tahun_lahir: 1992,
};
console.log(daftarPeserta);

// soal 2
var buah = [
  {
    nama: "strawberry",
    warna: "merah",
    ada_bijinya: "tidak",
    harga: 9000,
  },
  {
    nama: "jeruk",
    warna: "oranye",
    ada_bijinya: "ada",
    harga: 8000,
  },
  {
    nama: "Semangka",
    warna: "Hijau & Merah",
    ada_bijinya: "ada",
    harga: 10000,
  },
  {
    nama: "Pisang",
    warna: "Kuning",
    ada_bijinya: "tidak",
    harga: 5000,
  },
];
console.log(buah[0]);

// soal 3
var dataFilm = [];
function addFilm(nama, durasi, genre, tahun) {
  let film = {
    nama: nama,
    durasi: durasi,
    genre: genre,
    tahun: tahun,
  };
  dataFilm.push(film);
}
addFilm("Avenger", "120 Menit", "Action", 2017);
addFilm("Doctor Strange", "120 Menit", "Action", 2017);
addFilm("Captain America", "120 Menit", "Action", 2017);
console.log(dataFilm);

// soal 4
// Release 0
class Animal {
  constructor(name) {
    this._name = name;
    this._legs = 4;
    this._cold_blooded = false;
  }
  get legs() {
    return this._legs;
  }
  get cold_blooded() {
    return this._cold_blooded;
  }
  get name() {
    return this._name;
  }
}
var sheep = new Animal("shaun");
console.log(sheep.name);
console.log(sheep.legs);
console.log(sheep.cold_blooded);

// Release 1
class Ape extends Animal {
  constructor(name) {
    super(name);
    this._legs = 2;
  }
  yell() {
    return "Auooo";
  }
}
var sungokong = new Ape("kera sakti");
console.log(sungokong.name);
console.log(sungokong.legs);
console.log(sungokong.cold_blooded);
console.log(sungokong.yell());

class Frog extends Animal {
  jump() {
    return "Hop Hop";
  }
}
var kodok = new Frog("buduk");
console.log(kodok.name);
console.log(kodok.legs);
console.log(kodok.cold_blooded);
console.log(kodok.jump());

// Soal 5
class Clock {
  constructor({ template }) {
    this.template = template;
  }
  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = "0" + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = "0" + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = "0" + secs;

    var output = this.template
      .replace("h", hours)
      .replace("m", mins)
      .replace("s", secs);

    console.log(output);
  }
  stop() {
    clearInterval(this.timer);
  }
  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}

var clock = new Clock({ template: "h:m:s" });
clock.start();