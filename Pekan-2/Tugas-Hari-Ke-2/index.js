var readBooks = require("./callback");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

var i = 0;
function reading(time) {
  readBooks(time, books[i], function (waktu) {
    if (waktu >= books[i].timeSpent) {
      i++;
      reading(waktu);
    }
  });
}
reading(10000);
