var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

var i = 0;
function reading(time) {
  readBooksPromise(time, books[i])
  .then(function (waktu) {
    if (waktu >= books[i].timeSpent) {
        i++;
        reading(waktu);
    }
  })
  .catch(function (waktu) {
    if (waktu >= books[i].timeSpent) {
        i++;
        reading(waktu);
    }
  });
}
reading(10000);
